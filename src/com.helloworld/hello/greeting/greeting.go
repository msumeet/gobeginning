package greeting

import "fmt"

// MyOwnType is Similar to Class with only fields
type MyOwnType struct { // if MyOwnType M is capital then public if M is small then private
	name      string
	greetings string
}

// CallingFunctions Function
func CallingFunctions() {
	FunctionTakingFunction(PrintLine)
	FunctionTakingFunction(Print)
	myOwnType := MyOwnType{"Sumeet", "Hello"}
	ParameterMethod(myOwnType)

	MultipleParameter("Hello", "HHHHH", "HJAKSDHKJASHDJK")
	CustomType()
	WorkingWithConstant()
	Pointers()
	Variables()
}

// Printer : Declaring Function Type
type Printer func(string)

// FunctionTakingFunction Function
func FunctionTakingFunction(nameOfFunction Printer) {
	nameOfFunction("hello")
	nameOfFunction("Dear")
}

// PrintLine Function
func PrintLine(s string) {
	fmt.Println(s)
}

// Print Function
func Print(s string) {
	fmt.Print(s)
}

// MultipleParameter Function. three dots (...) represents multiple parameter
func MultipleParameter(firstParameter string, multiple ...string) {
	// numberOfParameter := len(multiple)
	fmt.Println(firstParameter)
	fmt.Println(multiple[0])
	fmt.Println(multiple[1])
}

// ParameterMethod Method
func ParameterMethod(myOwntype MyOwnType) {
	_, alternate := ChangeToString(myOwntype.greetings, myOwntype.name)
	// fmt.Println(message)
	fmt.Println(alternate)
}

// ChangeToString Function
func ChangeToString(firstPart, secondPart string) (message string, alternate string) {
	message = firstPart + " " + secondPart
	alternate = "Hey!!!" + secondPart
	return
}

// Constant Declarations
const (
	Pie      = 3.14
	Language = "Go"
)

// Enum Declaration
const (
	A = iota
	B
	C
)

// CustomType Function
func CustomType() {
	// var cust = MyOwnType{}
	// var cust = MyOwnType{"Sumeet", "Hello!"}
	var cust = MyOwnType{name: "Sumeet", greetings: "Hello!"}

	fmt.Println(cust.name)
	fmt.Println(cust.greetings)
}

// WorkingWithConstant Function
func WorkingWithConstant() {
	fmt.Println(A, B, C)

	fmt.Println(Pie)
	fmt.Println(Language)
}

// Pointers Function
func Pointers() {
	// Working with the pointers
	message := "Hello World!"
	// var messageForPointer string = "My Message"
	messageForPointer := "My Message"
	// var pointr *string = &messageForPointer
	pointr := &messageForPointer  // & get memory address of the variable
	fmt.Println(message, *pointr) // Use pointr to get memory address and * to get value
}

// Variables Function
func Variables() {
	//..String Decleration
	message := "Hello World!"
	var message2 string //If needed outside function
	var message3 = "This way is also Possible"
	//Declaring Multiple int at once
	a, b, c := 1, 2, 3

	fmt.Println(message)
	fmt.Println(message2)
	fmt.Println(message3)
	fmt.Println(a)
	fmt.Println(b)
	fmt.Println(c)
}
